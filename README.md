# Test Description #

Take look at images in the `expected results` folder.

You should make the necessary edits to index.html to make the page look like the mock images respectively on Desktop screens (Desktop.jpg) and on Mobile screens (Mobile.jpg).

Please send result zipped to hiring@ewebresults.com.

Alternatively you can fork [this starter fiddle](https://jsfiddle.net/ewebresults/sxwzm5r8/), make the necessary edits and send us the link. This would be even better.

Good luck!

Hint: attention to details is always a good thing.